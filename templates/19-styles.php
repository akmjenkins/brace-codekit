<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Styles</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">
		
			<div class="article-body">
		
				<div class="section-header page-title">
					<h1 class="section-title">Styles</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="grid collapse-800">
					<div class="col col-2">
						<div class="item">
						
							<h1>H1 - 32px LATO REGULAR</h1>
							<h2>H2 - 28px LATO REGULAR</h2>
							<h3>H3 - 24px  LATO REGULAR</h3>
							<h4>H4 - 20px LATO REGULAR</h4>
							<h5>H5 - 16px LATO REGULAR</h5>
							<h6>H6 - 14px LATO REGULAR</h6>
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2">
						<div class="item">
							
							<p class="excerpt">
								16px Lato Regular - Maecenas aliquet vehicula dui, eu elementum sapien porta vitae. Vestibulum sed massa quis est molestie tristique a at nisl. Fusce gravida egestas magna et interdum. Aenean commodo faucibus risus, sit amet imperdiet nisi. Curabitur vel ipsum quam. Suspendisse a mi vel nisl dignissim pharetra eu et libero.
							</p>
							
							<p>
								14px Lato Regular - Vivamus ultricies nibh lorem, et rhoncus ligula dapibus non. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce feugiat vulputate enim, a facilisis neque tincidunt eget. In suscipit, leo pellentesque blandit varius, nisl neque pellentesque elit, eget mattis eros mauris sed mi. Cras dignissim placerat sapien ut tincidunt. Aliquam erat volutpat. Morbi porta ex sem, congue mattis odio lacinia non. Sed faucibus pellentesque sem, ut consequat erat mollis a. Nunc velit elit, lobortis id iaculis id, aliquet nec enim. Sed lacus nibh, maximus ac eros ac, blandit congue mi. Cras et risus et felis maximus ultricies nec in lectus. Curabitur nec faucibus orci, quis facilisis ex.
							</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->

				<hr />
				
				<div class="grid collapse-900">
					<div class="col-2 col">
						<div class="item">
							<h3>Buttons</h3>
							<br />
							<a href="#" class="button green">Button Normal</a>
							<a href="#" class="button green hovered">Button Hover</a>
							<a href="#" class="button green disabled">Button Disabled</a>
							
							<br />
							<br />
							
							<h3>Link Colors</h3>
							<h6>(Inline only - inside paragraphs, blockquotes, lists, or with a class of "inline")</h6>
							
							<br />
									
								<p>
								
									<a href="#linky" class="uc">a:link</a>
									
									<br />
									
									<a href="#hovered" class="uc hover">a:hover</a>
									
									<br />
									
									<a href="#" class="uc visited">a:visited</a>
								
								</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col-2 col">
						<div class="item">
							<h3>Social Icons</h3>
							
							<div class="grid collapse-600">
								<div class="col col-2">
									<div class="item">
									
										<br />
									
										<div class="social light-social">
											
											<a href="#" class="fa fa-facebook">Facebook</a>
											<a href="#" class="fa fa-twitter">Twitter</a>
											<a href="#" class="fa fa-instagram">Instagram</a>
											<a href="#" class="fa fa-linkedin">LinkedIn</a>
											<a href="#" class="fa fa-youtube">YouTube</a>
											<a href="#" class="fa fa-google-plus">GooglePlus</a>
											<a href="#" class="fa fa-flickr">Flickr</a>
											
										</div><!-- .social -->
										
										<div class="social dark-social hovered">
										
											<a href="#" class="fa fa-facebook">Facebook</a>
											<a href="#" class="fa fa-twitter">Twitter</a>
											<a href="#" class="fa fa-instagram">Instagram</a>
											<a href="#" class="fa fa-linkedin">LinkedIn</a>
											<a href="#" class="fa fa-youtube">YouTube</a>
											<a href="#" class="fa fa-google-plus">GooglePlus</a>
											<a href="#" class="fa fa-flickr">Flickr</a>
											
										</div><!-- .social -->
										
										<br />
										
									</div><!-- .item -->
								</div><!-- .col -->
								<div class="col col-2">
									<div class="item black-bg">
									
										<br />
									
										<div class="social dark-social">
										
											<a href="#" class="fa fa-facebook">Facebook</a>
											<a href="#" class="fa fa-twitter">Twitter</a>
											<a href="#" class="fa fa-instagram">Instagram</a>
											<a href="#" class="fa fa-linkedin">LinkedIn</a>
											<a href="#" class="fa fa-youtube">YouTube</a>
											<a href="#" class="fa fa-google-plus">GooglePlus</a>
											<a href="#" class="fa fa-flickr">Flickr</a>
											
										</div><!-- .social -->

										<div class="social dark-social hovered">
										
											<a href="#" class="fa fa-facebook">Facebook</a>
											<a href="#" class="fa fa-twitter">Twitter</a>
											<a href="#" class="fa fa-instagram">Instagram</a>
											<a href="#" class="fa fa-linkedin">LinkedIn</a>
											<a href="#" class="fa fa-youtube">YouTube</a>
											<a href="#" class="fa fa-google-plus">GooglePlus</a>
											<a href="#" class="fa fa-flickr">Flickr</a>
											
										</div><!-- .social -->

										<br />
										
									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->
							
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
				
				<hr />
				
				<div class="grid collapse-800">
					<div class="col-2 col">
						<div class="item">
							<h3>Unordered List</h3>
							
							<ul>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
									</ul>
								</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</li>
							</ul>
							
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col-2 col">
						<div class="item">
							<h3>Ordered List</h3>
							
							<ol>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									<ol>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
									</ol>
								</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</li>
							</ol>
							
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
				
				
				
			</div><!-- .article-body -->
			
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>