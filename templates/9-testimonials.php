<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">The Latest</a>
			<a href="#">Testimonials</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">Testimonials</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="filter-area">
			
				<div class="filter-bar">
					
						<div class="filter-controls">
							<button class="prev fa fa-angle-left">Previous</button>
							<button class="next fa fa-angle-right">Next</button>
						</div><!-- .filter-controls -->
						
						<div class="count">
							<span class="num">13</span>
							Testimonials
						</div><!-- .count -->
						
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					
					<div class="grid eqh collapse-no-flex blocks">
					
						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
									Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
									Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
									Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
									Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-1">
							<div class="item">
							
								<div class="block">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>							
									<span class="cite">&mdash; Satisfied Customer</span>
								</div><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid.eqh -->

					
				</div><!-- .filter-content -->
				
			</div><!-- .filter-area -->

		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>