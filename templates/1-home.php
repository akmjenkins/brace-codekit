<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-home-hero.php'); ?>

<div class="body">

	<section class="dark-bg center" style="background-image: url(../assets/images/temp/knee-bg.jpg);">
		<div class="sw">
			
			<div class="section-header">
				<span class="section-title h1-style">Suffering From Injury?</span>
				<span class="subtitle">Use our injury finder tool to find products and tips to get you back on your feet.</span>
			</div><!-- .section-header -->
			
			<a href="#" class="button green">Use Our Injury Finder</a>
			
		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
		
			<div class="section-header">
				<span class="section-title h1-style">Coaching Help</span>
				<span class="subtitle">Whether you are interested in coaching or looking for a coach, we can help.</span>
			</div><!-- .section-header -->
			
			<div class="looking-for-help">
			
				<div class="blue-bg looking-for-help-form dark-bg">
					<div class="grid vcenter collapse-700">
						<div class="col-2 col">
							<div class="item">
								<h3>Looking for help?</h3>
								<p>Use the search box to help find what you are looking for.</p>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2 col">
							<div class="item">						
								<form action="/" method="post" class="single-form">
									<fieldset>
										<input type="text" name="help" placeholder="Type your search here...">
										<button class="fa fa-chevron-circle-right" type="submit">Search</button>
									</fieldset>
								</form><!-- .single-form -->
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .blue-bg -->
				
				<div class="green-bg dark-bg full-bg">

					<div class="grid eqh collapse-no-flex collapse-750 blocks nopad clear centered">
					
						<div class="col-4 col">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="fa fa-check-square">&nbsp;</div>
									</div><!-- .img-wrap -->
									<div class="content">
										<h5>Why You Need a Coach</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										<span class="button blue fa fa-chevron-circle-right">
											More Info
										</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-4 col">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="fa fa-arrows-h">&nbsp;</div>
									</div><!-- .img-wrap -->
									<div class="content">
										<h5>1-on-1 Coaching</h5>
										<p>Lorem ipsum dolor sit amet...</p>
										<span class="button blue fa fa-chevron-circle-right">
											More Info
										</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-4 col">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="fa fa-tachometer">&nbsp;</div>
									</div><!-- .img-wrap -->
									<div class="content">
										<h5>Performance Management</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										<span class="button blue fa fa-chevron-circle-right">
											More Info
										</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-4 col">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="fa fa-question-circle">&nbsp;</div>
									</div><!-- .img-wrap -->
									<div class="content">
										<h5>Coaching Advice</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										<span class="button blue fa fa-chevron-circle-right">
											More Info
										</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid.eqh -->
				</div><!-- .green-bg -->
			
			</div><!-- .looking-for-help -->
			
		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">
			
			<div class="section-header">
				<span class="section-title h1-style">Products to Help</span>
				<span class="subtitle">Check out our full line of products to get you back on your feet and doing what you love.</span>
			</div><!-- .section-header -->
			
			<div class="grid eqh collapse-no-flex blocks collapse-800 centered">
			
				<div class="col-4 col">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/blocks-1.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<h5>Body Parts</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green fa fa-chevron-circle-right">
									More Info
								</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 col">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/blocks-2.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<h5>Exercise Equipment</h5>
								<p>Lorem ipsum dolor sit amet...</p>
								<span class="button green fa fa-chevron-circle-right">
									More Info
								</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 col">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/blocks-3.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<h5>Running Gear</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<span class="button green fa fa-chevron-circle-right">
									More Info
								</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 col">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/blocks-4.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<h5>Fueling Up</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green fa fa-chevron-circle-right">
									More Info
								</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid.eqh -->
			
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>