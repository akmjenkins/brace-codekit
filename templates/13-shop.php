<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Shop</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<?php include('inc/i-shop-menu.php'); ?>
	
	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">Shop</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->

			<h3>Featured Items</h3>
			
			<div class="grid eqh collapse-no-flex blocks collapse-500">
			
				<div class="col-4 col sm-col-2">
					<div class="item">
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<h4>Shop Item One</h4>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								
								<span class="price">CAD $99.99</span>
								
								<span class="button green">More Info</span>
								
								<span class="category">Category</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-4 col sm-col-2">
					<div class="item">
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<h4>Shop Item One</h4>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								
								<span class="price">CAD $99.99</span>
								
								<span class="button green">More Info</span>
								
								<span class="category">Category</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-4 col sm-col-2">
					<div class="item">
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<h4>Shop Item One</h4>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								
								<span class="price">CAD $99.99</span>
								
								<span class="button green">More Info</span>
								
								<span class="category">Category</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-4 col sm-col-2">
					<div class="item">
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<h4>Shop Item One</h4>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								
								<span class="price">CAD $99.99</span>
								
								<span class="button green">More Info</span>
								
								<span class="category">Category</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid.eqh -->

		</div><!-- .sw -->
	</section>
	
	<section id="injury-finder">
	
		<section class="light-bg">
			<div class="sw">
				
				<div class="section-header">
					<span class="h3-style">Shop by Body Part</span>
					<span class="subtitle wide">Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, ultricies blandit ipsum.</span>
				</div><!-- .section-header -->
				
				<div class="selector" id="injury-finder-selector">
					<select>
						<option value="Back">Back</option>
						<option value="Hip">Hip</option>
						<option value="Knee">Knee</option>
						<option value="Foot/Ankle">Foot/Ankle</option>
					</select><!-- .#injury-selector -->
					<span class="value fa fa-angle-down">&nbsp;</span>
				</div><!-- .selector -->
				
				<div class="grid" id="injury-finder-buttons">
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/back.svg" alt="Back Icon">
								<span>Back</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/hip.svg" alt="Hip Icon">
								<span>Hip</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/knee.svg" alt="Knee Icon">
								<span>Knee</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/foot.svg" alt="Foot Icon">
								<span>Foot/Ankle</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
			</div><!-- .sw -->
		</section>
	
	</section><!-- #injury-finder -->
	
	<section>
		<div class="sw">
			<div class="grid eqh centered collapse-no-flex blocks collapse-600">
			
				<div class="col-3 col">
					<div class="item shop-block">
						<a class="block with-button" href="#">
							<div class="content">
								<h3 class="shop-block-title">Exercise Equipment</h3>								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">More Info</span>
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-3 col">
					<div class="item shop-block">
						<a class="block with-button" href="#">
							<div class="content">
								<h3 class="shop-block-title">Running Gear</h3>								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<span class="button green">More Info</span>
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-3 col">
					<div class="item shop-block">
						<a class="block with-button" href="#">
							<div class="content">
								<h3 class="shop-block-title">Fueling Up</h3>								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<span class="button green">More Info</span>
							</div><!-- .content -->
						</a><!-- .block -->
					</div><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .grid.eqh -->
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>