<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Shop</a>
			<a href="#">Body Parts</a>
			<a href="#">Back</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">		
			<div class="section-header page-title">
				<h1 class="section-title">Back</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="filter-area">
			
				<div class="filter-bar">
					
						<div class="filter-controls">
							<button class="prev fa fa-angle-left">Previous</button>
							<button class="next fa fa-angle-right">Next</button>
						</div><!-- .filter-controls -->
						
						<div class="filter-bar-buttons">
							<a href="#" class="button green">Continue Shopping</a>
							<a href="#" class="button green">Checkout</a>
						</div><!-- ,.filter-bar-buttons -->
						
						<div class="count">
							<span class="num">10</span>
							Items Found
						</div><!-- .count -->
						
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					
					<div class="grid eqh collapse-no-flex blocks collapse-500">
					
						<div class="col-4 col sm-col-2">
							<div class="item cart-item">
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<h4>Shop Item One</h4>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										
										<span class="price">CAD $99.99</span>
										
										<span class="button green">More Info</span>
										
										<span class="category">Category</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								<button class="remove-link fa fa-times">Remove From Cart</button>
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-4 col sm-col-2">
							<div class="item cart-item">
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<h4>Shop Item One</h4>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										
										<span class="price">CAD $99.99</span>
										
										<span class="button green">More Info</span>
										
										<span class="category">Category</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								<button class="remove-link fa fa-times">Remove From Cart</button>
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-4 col sm-col-2">
							<div class="item cart-item">
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<h4>Shop Item One</h4>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										
										<span class="price">CAD $99.99</span>
										
										<span class="button green">More Info</span>
										
										<span class="category">Category</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								<button class="remove-link fa fa-times">Remove From Cart</button>
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-2">
							<div class="item cart-item">
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<h4>Shop Item One</h4>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
										
										<span class="price">CAD $99.99</span>
										
										<span class="button green">More Info</span>
										
										<span class="category">Category</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								<button class="remove-link fa fa-times">Remove From Cart</button>
							</div><!-- .item -->
						</div><!-- .col -->

						
					</div><!-- .grid.eqh -->
					
				</div><!-- .filter-content -->
				
			</div><!-- .filter-area -->

			
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>