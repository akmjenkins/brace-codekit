<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Coaching</a>
			<a href="#">Coaching Portal Login</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section class="article-body">
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">Coaching Portal Login</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->

			<div class="grid pad40">
				<div class="col-2 col sm-col-1">
					<div class="item">
						
						<h2>New User Signup</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
						
						<form action="/" method="post" class="body-form">
							<fieldset>
							
								<div class="form-message">
									Fill out the form to contact us
								</div>
								
								<div class="form-message blue">
									Example Form Message with a class of "blue"
								</div>
								
								<div class="form-message green">
									Example Form Message with a class of "green"
								</div>
								
								<div class="form-message error">
									Example Form Message with a class of "error"
								</div>
							
								<label class="placeholder-label" for="name">Name</label>
								<input type="text" placeholder="Name" name="name">
								
								<label class="placeholder-label" for="email">E-mail Address</label>
								<input type="email" placeholder="E-mail Address" name="email">
								
								<label class="placeholder-label" for="message">Password</label>
								<textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>
								
								<button type="submit" class="button green">Submit</button>
								
							</fieldset>
						</form><!-- .body-form -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col-2 col sm-col-1">
					<div class="item">

						<h4>Location</h4>
						
						<address>
							123 This Street <br />
							St. John's, NL    A1B 0C3
						</address>
						
						<br />
						
						<span class="block">p. 709 123 4567</span>
						<span class="block">f.  709 123 8910</span>
						
						<br />
						<br />
						
						<h4>Connect</h4>
						<div class="social light-social alleft">
							<a href="#" class="fa fa-facebook">Facebook</a>
							<a href="#" class="fa fa-twitter">Twitter</a>
							<a href="#" class="fa fa-youtube">YouTube</a>
							<a href="#" class="fa fa-instagram">Instagram</a>
							<a href="#" class="fa fa-google-plus">GooglePlus</a>
						</div><!-- .social -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<div class="gmap">
		<div class="map" data-center="47.5605413,-52.7128315" data-zoom="10" data-markers='[{"title":"St. Johns, NL","position":"47.5605413,-52.7128315"}]'></div>
	</div>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>