<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Coaching</a>
			<a href="#">Coachng Advice</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Coaching Advice</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body with-sidebar">
						<div class="article-body">

							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, 
								ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, ante orci venenatis tellus, sit amet lobortis eros 
								sapien nec urna. Aenean semper, justo at molestie porttitor, est mi tincidunt metus, quis fermentum erat justo non sapien. In eget purus congue, aliquam mauris nec, 
								sollicitudin sem. Curabitur interdum magna sed dignissim sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac cursus urna. Vivamus non urna 
								eget mi commodo consectetur eget et leo. Nunc dignissim vehicula dui a elementum. Nulla non arcu quis massa consequat consectetur vehicula in turpis.
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						
						<div class="side-nav">
							<span class="title">Navigation</span>
							<a href="#">Coaching Portal Login</a>
							<a href="#">Performance Management</a>
							<a href="#">Coaching Advice</a>
							<a href="#">1-On-1 Coaching</a>
							<a href="#" class="selected">Why You Need a Coach</a>
						</div><!-- .side-nav -->
						
					</aside><!-- .sidebar -->
				</div><!-- .cf -->
				
			<div class="overview-blocks">
				
				<div class="overview-block with-img light-bg">
				
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-1.jpg);">
							<img src="../assets/images/temp/ov-img-1.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">Reason One</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
						
					</div><!-- .content -->
					
				</div><!-- .overview-block -->
				
				<div class="overview-block with-img light-bg">
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">Reason Two</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor.</p>
						
					</div><!-- .content -->
					
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-2.jpg);">
							<img src="../assets/images/temp/ov-img-2.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
					
				</div><!-- .overview-block -->
				
				<div class="overview-block with-img light-bg">
				
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-3.jpg);">
							<img src="../assets/images/temp/ov-img-3.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">Reason Three</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
						
					</div><!-- .content -->
					
				</div><!-- .overview-block -->
				
			</div>

				
			</div><!-- .sw -->
		</section>

	</article>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>