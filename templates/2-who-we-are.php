<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">Who We Are</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
			
			<div class="overview-blocks">
				
				<a class="overview-block with-img light-bg" href="#">
				
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-1.jpg);">
							<img src="../assets/images/temp/ov-img-1.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">My Story</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
						
						<span class="button green">Read More</span>
					</div><!-- .content -->
					
				</a><!-- .overview-block -->
				
				<a class="overview-block with-img light-bg" href="#">
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">Our Team</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor.</p>
						
						<span class="button green">Read More</span>
					</div><!-- .content -->
					
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-2.jpg);">
							<img src="../assets/images/temp/ov-img-2.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
					
				</a><!-- .overview-block -->
				
				<a class="overview-block with-img light-bg" href="#">
				
					<div class="img-wrap" >
						<div style="background-image: url(../assets/images/temp/ov-img-3.jpg);">
							<img src="../assets/images/temp/ov-img-3.jpg" alt="alt text">
						</div>
					</div><!-- .img-wrap -->
				
					<div class="content article-body">
						<div class="hgroup">
							<span class="h1-style">FAQs</span>
							<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
						
						<span class="button green">Read More</span>
					</div><!-- .content -->
					
				</a><!-- .overview-block -->
				
			</div>
			
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>