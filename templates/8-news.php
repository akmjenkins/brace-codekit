<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">The Latest</a>
			<a href="#">News</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">News</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="filter-area">
			
				<div class="filter-bar">
					
						<div class="filter-controls">
							<button class="prev fa fa-angle-left">Previous</button>
							<button class="next fa fa-angle-right">Next</button>
						</div><!-- .filter-controls -->
						
						<div class="count">
							<span class="num">10</span>
							Articles
						</div><!-- .count -->
						
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					
					<div class="grid eqh collapse-no-flex blocks collapse-500">
					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-img with-button" href="#">
									<div class="img-wrap">
										<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<time datetime="2014-10-02">October 2, 2014</time>
									
										<div class="hgroup">
											<h2>News Item One</h2>
											<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
										</div><!-- .hgroup -->
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										
										<span class="button green">Read More</span>
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid.eqh -->
					
				</div><!-- .filter-content -->
				
			</div><!-- .filter-area -->

		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>