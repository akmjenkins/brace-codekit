<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Shop</a>
			<a href="#">Body Parts</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<?php include('inc/i-shop-menu.php'); ?>


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Body Parts</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body">
						<div class="article-body">
						
							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, 
								ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, ante orci venenatis tellus, sit amet lobortis eros 
								sapien nec urna. Aenean semper, justo at molestie porttitor, est mi tincidunt metus, quis fermentum erat justo non sapien.
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .cf -->
				
			</div><!-- .sw -->
		</section>

	</article>
	
	<section>
	
		<section class="light-bg">
			<div class="sw">
			
				<div class="selector" id="injury-finder-selector">
					<select>
						<option value="Back">Back</option>
						<option value="Hip">Hip</option>
						<option value="Knee">Knee</option>
						<option value="Foot/Ankle">Foot/Ankle</option>
					</select><!-- .#injury-selector -->
					<span class="value fa fa-angle-down">&nbsp;</span>
				</div><!-- .selector -->
				
				<div class="grid" id="injury-finder-buttons">
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/back.svg" alt="Back Icon">
								<span>Back</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/hip.svg" alt="Hip Icon">
								<span>Hip</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/knee.svg" alt="Knee Icon">
								<span>Knee</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<a href="#" class="injury-finder-button">
								<img src="../assets/images/foot.svg" alt="Foot Icon">
								<span>Foot/Ankle</span>
							</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
			</div><!-- .sw -->
		</section>
	
	</section><!-- #injury-finder -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>