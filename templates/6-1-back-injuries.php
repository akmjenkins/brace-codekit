<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Common Injuries</a>
			<a href="#">Back Injuries</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Back Injuries</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body with-sidebar">
						<div class="article-body">
						
							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, 
								ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, ante orci venenatis tellus, sit amet lobortis eros sapien 
								nec urna. Aenean semper, justo at molestie porttitor, est mi tincidunt metus, quis fermentum erat justo non sapien. In eget purus congue, aliquam mauris nec, sollicitudin 
								sem. Curabitur interdum magna sed dignissim sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac cursus urna. Vivamus non urna eget mi commodo 
								consectetur eget et leo. Nunc dignissim vehicula dui a elementum. Nulla non arcu quis massa consequat consectetur vehicula in turpis.
							</p>
 
							<p>
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi malesuada tortor nec nulla dignissim, sed aliquet risus scelerisque. In metus 
								nulla, sollicitudin vitae felis euismod, suscipit eleifend libero. Fusce ac diam eget mi imperdiet congue. Etiam maximus nec lacus sed semper. Aenean tempor pharetra 
								metus quis aliquet. Morbi turpis leo, hendrerit sed ligula id, mattis facilisis nibh. Pellentesque venenatis, nisi nec auctor faucibus, ipsum justo pulvinar lectus, id 
								malesuada velit sapien nec nunc. Aenean et felis posuere, efficitur tortor quis, aliquet felis. Nunc ultricies fermentum ex, at malesuada ligula consequat sit amet.
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						
						<div class="side-nav">
							<span class="title">Navigation</span>
							<a href="#">Injury Finder Tool</a>
							<a href="#" class="selected">Back Injuries</a>
							<a href="#">Hip Injuries</a>
							<a href="#">Knee Injuries</a>
							<a href="#">Foot/Ankle Injuries</a>
						</div><!-- .side-nav -->
						
					</aside><!-- .sidebar -->
				</div><!-- .cf -->
				
			</div><!-- .sw -->
		</section>

	</article>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>