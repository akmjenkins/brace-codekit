<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">The Latest</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">The Latest</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="light-bg">
		<div class="sw">
		
			<div class="section-header page-title">
				<h3 class="section-title">Latest News</h3>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="grid eqh collapse-no-flex blocks collapse-500">
			
				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<time datetime="2014-10-02">October 2, 2014</time>
							
								<div class="hgroup">
									<h2>News Item One</h2>
									<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<time datetime="2014-10-02">October 2, 2014</time>
							
								<div class="hgroup">
									<h2>News Item One</h2>
									<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<a class="block with-img with-button" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<time datetime="2014-10-02">October 2, 2014</time>
							
								<div class="hgroup">
									<h2>News Item One</h2>
									<span class="subtitle">Lorem ipsum dolor sit amet consectetur</span>
								</div><!-- .hgroup -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid.eqh -->
			
			<div class="center">
				<a href="#" class="button green">More News</a>
			</div><!-- .centered -->

			
		</div><!-- .sw -->
	</section><!-- .light-bg -->
	
	<section>
		<div class="sw">
		
			<div class="section-header page-title">
				<h3 class="section-title">Latest Testimonials</h3>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="grid eqh collapse-no-flex blocks">
			
				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<div class="block">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
							Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
							<span class="cite">&mdash; Satisfied Customer</span>
						</div><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<div class="block">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>							
							<span class="cite">&mdash; Satisfied Customer</span>
						</div><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-3 col sm-col-1">
					<div class="item">
					
						<div class="block">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>							
							<span class="cite">&mdash; Satisfied Customer</span>
						</div><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid.eqh -->
			
			<div class="center">
				<a href="#" class="button green">More Testimonials</a>
			</div><!-- .centered -->
			
		</div><!-- .sw -->
	</section><!-- .light-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>