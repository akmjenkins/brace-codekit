<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Shop</a>
			<a href="#">Account</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<?php include('inc/i-shop-menu.php'); ?>
	
	<section>
		<div class="sw">		
			<div class="section-header page-title">
				<h1 class="section-title">Account</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->
			
			<div class="cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
					
						<h2>Your Information</h2>
						
						<address>
							John Smith <br />
							123 Street Address <br />
							St. John's, NL     A1B 2C3
						</address>
	 
							<br />
	 
						<span class="block">Phone  709.123.4567</span>
						<span class="block">email me@thisdomain.com</span>
						
						<hr />
						
						<h3>Active Order</h3>
						
						<table class="responsive">
							<thead>
								<tr>
									<th>Order No.</th>
									<th>Date</th>
									<th>Status</th>
									<th>Amount</th>
									<th>Shipping</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><a href="#">02929292</a></td>
									<td>October 3, 2014</td>
									<td>Processing</td>
									<td>$205.39</td>
									<td>Standard</td>
								</tr>
								<tr>
									<td><a href="#">01948795</a></td>
									<td>September 30, 2014</td>
									<td>Shipped</td>
									<td>$102.46</td>
									<td>Standard</td>
								</tr>
							</tbody>
						</table><!-- .responsive -->
						
						<h3>Order History</h3>
						
						<table class="responsive">
							<thead>
								<tr>
									<th>Order No.</th>
									<th>Date</th>
									<th>Status</th>
									<th>Amount</th>
									<th>Shipping</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><a href="#">02929292</a></td>
									<td>October 3, 2014</td>
									<td>Processing</td>
									<td>$205.39</td>
									<td>Standard</td>
								</tr>
								<tr>
									<td><a href="#">01948795</a></td>
									<td>September 30, 2014</td>
									<td>Shipped</td>
									<td>$102.46</td>
									<td>Standard</td>
								</tr>
								<tr>
									<td><a href="#">00293884</a></td>
									<td>September 5, 2014</td>
									<td>Cancelled</td>
									<td>$205.39</td>
									<td>Standard</td>
								</tr>
							</tbody>
						</table><!-- .responsive -->
					
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
				
					<div class="recently-viewed">
					
						<h4 class="title">Recently Viewed</h4>
					
						<a class="recently-viewed-product" href="#">
							<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
							
							<span class="product">
								<span class="title">Running Shoes</span>
								<span class="price">$139.99</span>
							</span>
							
							<span class="link">View</span>
						</a><!-- .recently-viewed-product -->
						
						<a class="recently-viewed-product" href="#">
							<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
							
							<span class="product">
								<span class="title">Running Shoes</span>
								<span class="price">$139.99</span>
							</span>
							
							<span class="link">View</span>
						</a><!-- .recently-viewed-product -->
						
						<a class="recently-viewed-product" href="#">
							<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
							
							<span class="product">
								<span class="title">Running Shoes</span>
								<span class="price">$139.99</span>
							</span>
							
							<span class="link">View</span>
						</a><!-- .recently-viewed-product -->
						
						<a href="#" class="right uc">View All</a>
					</div>
				
				</aside><!-- .sidebar -->
			</div><!-- .cf -->
			
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>