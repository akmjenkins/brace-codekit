<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Coaching</a>
			<a href="#">Coachng Advice</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Coaching Advice</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body with-sidebar">
						<div class="article-body">

							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, 
								ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, ante orci venenatis tellus, sit amet lobortis eros 
								sapien nec urna. Aenean semper, justo at molestie porttitor, est mi tincidunt metus, quis fermentum erat justo non sapien. In eget purus congue, aliquam mauris nec, 
								sollicitudin sem. Curabitur interdum magna sed dignissim sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac cursus urna. Vivamus non urna 
								eget mi commodo consectetur eget et leo. Nunc dignissim vehicula dui a elementum. Nulla non arcu quis massa consequat consectetur vehicula in turpis.
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						
						<div class="side-nav">
							<span class="title">Navigation</span>
							<a href="#">Coaching Portal Login</a>
							<a href="#">Performance Management</a>
							<a href="#" class="selected">Coaching Advice</a>
							<a href="#">1-On-1 Coaching</a>
							<a href="#">Why You Need a Coach</a>
						</div><!-- .side-nav -->
						
					</aside><!-- .sidebar -->
				</div><!-- .cf -->
				
			</div><!-- .sw -->
		</section>

	</article>
	
	<section>
		<div class="sw">
			<div class="looking-for-help">
				<div class="blue-bg looking-for-help-form dark-bg">
					<div class="grid vcenter collapse-700">
						<div class="col-2 col">
							<div class="item">
								<h3>Looking for help?</h3>
								<p>Use the search box to help find what you are looking for.</p>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2 col">
							<div class="item">						
								<form action="/" method="post" class="single-form">
									<fieldset>
										<input type="text" name="help" placeholder="Type your search here...">
										<button class="fa fa-chevron-circle-right" type="submit">Search</button>
									</fieldset>
								</form><!-- .single-form -->
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .blue-bg -->
			</div><!-- .looking-for-help -->
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">

			<div class="section-header page-title">
				<h3 class="section-title">FAQs</h3>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->		
		
			<div class="accordion">
			
				<div class="accordion-item">
					<div class="accordion-item-handle fa fa-angle-up">
						Morbi eros mauris, vestibulum id turpis quis, pellentesque malesuada metus?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content article-body">
						<p>Mauris gravida orci ac purus laoreet varius. Praesent lobortis nisi venenatis pulvinar sollicitudin. Morbi sagittis sem et mattis venenatis. Donec tempor euismod feugiat. In eu orci a tellus pharetra bibendum et fringilla ex. Praesent pretium tortor risus, vitae blandit orci cursus at. In sit amet sapien tincidunt, dapibus metus ut, tempor dui. Nunc consectetur eget metus et dignissim.</p>
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle fa fa-angle-up">
						Morbi eros mauris, vestibulum id turpis quis, pellentesque malesuada metus?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content article-body">
						<p>Mauris gravida orci ac purus laoreet varius. Praesent lobortis nisi venenatis pulvinar sollicitudin. Morbi sagittis sem et mattis venenatis. Donec tempor euismod feugiat. In eu orci a tellus pharetra bibendum et fringilla ex. Praesent pretium tortor risus, vitae blandit orci cursus at. In sit amet sapien tincidunt, dapibus metus ut, tempor dui. Nunc consectetur eget metus et dignissim.</p>
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle fa fa-angle-up">
						Morbi eros mauris, vestibulum id turpis quis, pellentesque malesuada metus?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content article-body">
						<p>Mauris gravida orci ac purus laoreet varius. Praesent lobortis nisi venenatis pulvinar sollicitudin. Morbi sagittis sem et mattis venenatis. Donec tempor euismod feugiat. In eu orci a tellus pharetra bibendum et fringilla ex. Praesent pretium tortor risus, vitae blandit orci cursus at. In sit amet sapien tincidunt, dapibus metus ut, tempor dui. Nunc consectetur eget metus et dignissim.</p>
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
			</div><!-- .accordion -->
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>