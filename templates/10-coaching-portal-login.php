<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Coaching</a>
			<a href="#">Coaching Portal Login</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section class="article-body">
		<div class="sw">
		
			<div class="section-header page-title">
				<h1 class="section-title">Coaching Portal Login</h1>
				<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
			</div><!-- .section-header -->

			<div class="grid pad40">
				<div class="col-2 col sm-col-1">
					<div class="item">
						
						<h2>New User Signup</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
						
						<form action="/" method="post" class="body-form">
							<fieldset>
							
								<label class="placeholder-label" for="name">Full Name</label>
								<input type="text" placeholder="Full Name" name="name">
								
								<label class="placeholder-label" for="email">E-mail Address</label>
								<input type="email" placeholder="E-mail Address" name="email">
								
								<label class="placeholder-label" for="password">Password</label>
								<input type="password" placeholder="Password..." name="password">
								
								<label class="placeholder-label" for="confirm_password">Confirm Password</label>
								<input type="password" placeholder="Confirm Password..." name="confirm_password">
								
								<button type="submit" class="blue button">Sign Up</button>
								
							
							</fieldset>
						</form><!-- .body-form -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col-2 col sm-col-1">
					<div class="item">
						
						<h2>Current User Sign in</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
						
						<form action="/" method="post" class="body-form">
							<fieldset>
								
								<label class="placeholder-label" for="email">E-mail Address</label>
								<input type="email" placeholder="E-mail Address" name="email">
								
								<label class="placeholder-label" for="password">Password</label>
								<input type="password" placeholder="Password..." name="password">
								
								<span class="alright block right">Forgot your password? Click <a href="#">here</a> to reset it</span>
								
								<button type="submit" class="blue button">Log in</button>
								
							</fieldset>
						</form><!-- .body-form -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>