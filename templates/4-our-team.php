<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Who We Are</a>
			<a href="#">Our Team</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Our Team</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body with-sidebar">
						<div class="article-body">
						
							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, 
								ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, ante orci venenatis tellus, sit amet lobortis eros 
								sapien nec urna. Aenean semper, justo at molestie porttitor, est mi tincidunt metus, quis fermentum erat justo non sapien. In eget purus congue, aliquam mauris nec, 
								sollicitudin sem. Curabitur interdum magna sed dignissim sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac cursus urna. Vivamus non urna 
								eget mi commodo consectetur eget et leo. Nunc dignissim vehicula dui a elementum. Nulla non arcu quis massa consequat consectetur vehicula in turpis.
							</p>
							
							<div class="grid eqh collapse-no-flex blocks collapse-800">
							
								<div class="col-2 col">
									<div class="item">
									
										<a class="block with-img with-button" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/images/temp/team-member-1.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<div class="hgroup">
													<h2>Team Member</h2>
													<span class="subtitle">Position Title</span>
												</div><!-- .hgroup -->
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col -->
								
								<div class="col-2 col">
									<div class="item">
									
										<a class="block with-img with-button" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/images/temp/team-member-2.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<div class="hgroup">
													<h2>Team Member</h2>
													<span class="subtitle">Position Title</span>
												</div><!-- .hgroup -->
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col-2 col">
									<div class="item">
									
										<a class="block with-img with-button" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/images/temp/team-member-1.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<div class="hgroup">
													<h2>Team Member</h2>
													<span class="subtitle">Position Title</span>
												</div><!-- .hgroup -->
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col -->
								
								<div class="col-2 col">
									<div class="item">
									
										<a class="block with-img with-button" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/images/temp/team-member-2.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<div class="hgroup">
													<h2>Team Member</h2>
													<span class="subtitle">Position Title</span>
												</div><!-- .hgroup -->
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col -->
								
							</div><!-- .grid.eqh -->

							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						
						<div class="side-nav">
							<span class="title">Navigation</span>
							<a href="#">My Story</a>
							<a href="#" class="selected">Our Team</a>
							<a href="#">FAQs</a>
						</div><!-- .side-nav -->
						
						<a class="side-promo blue" href="#">
							<div>
								<span class="title">Staying Motivated can be tough.</span>
								<span class="tag">Get help from a coach today.</span>
								<span class="button green">Find Out More</span>
							</div>
						</a><!-- .side-promo -->
						
						<a class="side-promo green" href="#">
							<div>
								<span class="title">The right equipment for running is as important as willpower.</span>
								<span class="tag">Check out our shop now.</span>
							
								<span class="button blue">Find Out More</span>
							</div>
						</a><!-- .side-promo -->
						
					</aside><!-- .sidebar -->
				</div><!-- .cf -->
				
			</div><!-- .sw -->
		</section>

	</article>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>