<div class="hero">
	<div class="swiper-wrapper button-controls">
		<div class="swipe" data-controls="true" data-auto="7" data-links="true">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<div>
								<span class="title">
									<span class="lt">My</span>Story
								</span><!-- .title -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo.</p>
								
								<a href="#" class="button green fa fa-play-circle-o fa-left">
									Watch Video
								</a><!-- .button -->
							</div>
						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

				<div data-src="../assets/images/temp/hero/2.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<div>
								<span class="title">
									<span class="lt">Our</span>Products
								</span><!-- .title -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo.</p>
								
								<a href="#" class="button green">
									See Products
								</a><!-- .button -->
							</div>
						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

				<div data-src="../assets/images/temp/hero/3.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<div>
								<span class="title">
									<span class="lt">Get</span>Coaching
								</span><!-- .title -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo.</p>
								
								<a href="#" class="button green">
									More Info
								</a><!-- .button -->
							</div>
						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>


			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->