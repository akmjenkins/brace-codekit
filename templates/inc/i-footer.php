					
			<footer class="dark-bg">
			
				<div class="sw">
				
					<div class="footer-contact">
						<h6>Connect</h6>
						
						<div class="address-block">
							<address>
								123 This Street <br>
								This Town, NL A1B 2C3
							</address>
							
							<div class="phones">
								<span> 555.555.5555</span>
								<span> 555.555.5556</span>
							</div><!-- .phones -->
						</div><!-- .address-block -->
						
					</div><!-- .contact-box -->
				
					<div class="footer-nav">
						<ul>
							<li><a href="#">Who We Are</a></li>
							<li><a href="#">Common Injuries</a></li>
							<li><a href="#">First Steps</a></li>
							<li><a href="#">The Latest</a></li>
							<li><a href="#">Coaching</a></li>
							<li><a href="#">Shops</a></li>
						</ul>
					</div><!-- .footer-nav -->
					
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Brace For Performance Inc.</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Policies</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac" class="replace">JAC. We Create.</a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->

		<div class="search-bg"></div>		
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/brace-codekit/',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js?<?php echo time(); ?>"></script>
	</body>
</html>
