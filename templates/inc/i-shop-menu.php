<div class="shop-menu">

	<h5 class="title fa fa-navicon">Shop Navigation</h5>
	<div class="menu-content">
	
		<div class="shop-menu-section shop-menu-user padded">
			Welcome, <span>username</span>
		</div><!-- .shop-menu-section -->
		
		<div class="shop-menu-section padded">
			
			<a href="#" class="cart"><i class="fa fa-shopping-cart"></i> - 0 Items</a>
			
			<a href="#">Your Account</a>
			
		</div><!-- .shop-menu-section -->
		
		<div class="shop-menu-section">
			
			<span class="shop-menu-section-title">Body Parts</span>
			
			<a href="#" class="block-link">Back</a>
			<a href="#" class="block-link">Hip</a>
			<a href="#" class="block-link">Knee</a>
			<a href="#" class="block-link">Foot/Ankle</a>
			
		</div><!-- .shop-menu-section -->
		
		<div class="shop-menu-section">
		
		<a href="#" class="block-link">Exercise Equipment</a>
		<a href="#" class="block-link">Running Gear</a>
		<a href="#" class="block-link">Fueling Up</a>
		
	</div><!-- .shop-menu-section -->
	
	</div><!-- .menu-content -->
	
	
</div><!-- .shop-menu -->