<div class="nav-wrap">

	<?php include('inc/i-search.php'); ?>

	<div class="nav">
		<div class="sw">

			<nav>
				<ul>
					<li>
						<a href="#">
							<span>Who We Are</span>
						</a>

						<div>

							<ul>
								<li><a href="#">My Story</a></li>
								<li><a href="#">Our Team</a></li>
								<li><a href="#">FAQs</a></li>
							</ul>
							
							<div class="link-meta dark-bg">
							
								<div>
								
									<div class="meta">
										<h4>A Heading Referring to Who We Are</h4>
										<p>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, 
										nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
										felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-1.jpg">&nbsp;</div>
									
								</div>
							
								<div>
								
									<div class="meta">
										<h4>A Heading Referring to My Story</h4>
										<p>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, 
										nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
										felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-2.jpg">&nbsp;</div>
									
								</div>
								
								<div>
								
									<div class="meta">
										<h4>A Heading Referring to Our Team</h4>
										<p>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, 
										nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
										felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-3.jpg">&nbsp;</div>
									
								</div>
								
								<div>
								
									<div class="meta">
										<h4>A Heading Referring to FAQs</h4>
										<p>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
										Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, 
										nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
										felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-4.jpg">&nbsp;</div>
									
								</div>
								
								
								
							</div><!-- .meta -->

						</div>

					</li>
					<li>
						<a href="#">
							<span>Common Injuries</span>
						</a>
					</li>
					<li>
						<a href="#">
							<span>First Steps</span>
						</a>
					</li>
					<li>
						<a href="#">
							<span>The Latest</span>
						</a>
					</li>
					<li>
						<a href="#">
							<span>Coaching</span>
						</a>
					</li>
					<li class="shop">
						<a href="#">
							<span>
								<i class="fa fa-shopping-cart"></i>
								Shop
							</span>
						</a>
						
						<div class="shop-dropdown dark-bg">
							
							<div class="shop-dropdown-store">
								
								<div class="shop-dropdown-body-parts">
									<span class="h4-style title">Body Parts</span>
									
									<a href="#" class="body-part-link">
										<span><img src="../assets/images/back.svg" alt="Spine"></span>
										Back
									</a><!-- .body-part-link -->
									
									<a href="#" class="body-part-link">
										<span><img src="../assets/images/knee.svg" alt="Knee"></span>
										Knee
									</a><!-- .body-part-link -->
									
									<a href="#" class="body-part-link">
										<span><img src="../assets/images/hip.svg" alt="Hip"></span>
										Hip
									</a><!-- .body-part-link -->
									
									<a href="#" class="body-part-link">
										<span><img src="../assets/images/foot.svg" alt="Foot"></span>
										Foot/Ankle
									</a><!-- .body-part-link -->
								
								</div><!-- .shop-dropdown-body-parts -->
								
								<div class="shop-dropdown-store-links">
									<a href="#">Exercise Equipment</a>
									<a href="#">Running Gear</a>
									<a href="#">Fueling Up</a>
								</div><!-- .shop-dropdown-store-links -->
								
							</div><!-- .shop-dropdown-store -->
							
							<div class="shop-dropdown-account">
								
								<div class="shop-dropdown-cart">
								
									<span class="h4-style title">My Account</span>
									
									<div class="shop-dropdown-cart-contents">
										<span class="items">
											<i class="fa fa-shopping-cart"></i> - 0 Items
										</span>
										
										<span class="total">
											Total - $0.00
										</span>
									</div><!-- .shop-dropdown-cart-total -->
									
									<a href="#" class="button blue right">Checkout</a>
									
								</div><!-- .shop-dropdown-cart -->
								
								<div class="shop-dropdown-recently-viewed">
									<span class="h6-style title">Recently Viewed Products</span>

									<a class="recently-viewed-product" href="#">
										<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
										
										<span class="product">
											<span class="title">Running Shoes</span>
											<span class="price">$139.99</span>
										</span>
										
										<span class="link">View</span>
									</a><!-- .recently-viewed-product -->
									
									<a class="recently-viewed-product" href="#">
										<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
										
										<span class="product">
											<span class="title">Running Shoes</span>
											<span class="price">$139.99</span>
										</span>
										
										<span class="link">View</span>
									</a><!-- .recently-viewed-product -->
									
									<a class="recently-viewed-product" href="#">
										<span class="product-img" style="background-image: url(../assets/images/temp/product.jpg);"></span>
										
										<span class="product">
											<span class="title">Running Shoes</span>
											<span class="price">$139.99</span>
										</span>
										
										<span class="link">View</span>
									</a><!-- .recently-viewed-product -->
								</div>
								
							</div><!-- .shop-dropdown-account -->
							
						</div><!-- .shop-dropdown -->
					</li>
				</ul>
			</nav>

			<div class="top">

				<?php include('i-social.php'); ?>
				
				<div>
				
					<a href="#">Coaching Portal</a>
					<a href="#">Account</a>
				
				</div>
				
				<div>
					<a href="#" class="shop">
						<i class="fa fa-shopping-cart"></i>
						0 Items
					</a>
				</div>

			</div><!-- .top -->
			
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->