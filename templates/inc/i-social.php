<div class="social">

	<a href="#" title="Like Brace for Performance on Facebook" class="fa fa-facebook" rel="external">Like Brace for Performance on Facebook</a>
	<a href="#" title="Follow Brace for Performance on Twitter" class="fa fa-twitter" rel="external">Follow Brace for Performance on Twitter</a>
	<button class="search fa fa-search">Search</button>
	
	<span class="phone fa fa-phone">
		
		<span class="phone-tooltip">
			<span>
				<i class="fa fa-phone"></i>
				1.555.555.5555
			</span>
		</span><!-- .phone-tooltip -->
		
	</span><!-- .phone -->
	
</div><!-- .social -->