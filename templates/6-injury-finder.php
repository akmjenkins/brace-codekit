<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-inner-hero.php'); ?>

<div class="body">

	<div class="breadcrumbs light-bg">
		<div class="sw">
			<a href="#" class="fa fa-home">Home</a>
			<a href="#">Common Injuries</a>
			<a href="#">Injury Finder Tool</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->


	<article>
	
		<section>
			<div class="sw">
			
				<div class="section-header page-title">
					<h1 class="section-title">Injury Finder Tool</h1>
					<span class="subtitle">Pellentesque Habitant Morbi Tristique</span>
				</div><!-- .section-header -->
				
				<div class="cf">
					<div class="main-body with-sidebar">
						<div class="article-body">
						
							<p class="excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
								felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
							<p>
								Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. 
								Phasellus quam lectus, luctus a massa a, ultricies blandit ipsum. Ut auctor pellentesque mattis. Phasellus pellentesque, massa posuere blandit fermentum, 
								ante orci venenatis tellus, sit amet lobortis eros sapien nec urna.
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						
						<div class="side-nav">
							<span class="title">Navigation</span>
							<a href="#" class="selected">Injury Finder Tool</a>
							<a href="#">Back Injuries</a>
							<a href="#">Hip Injuries</a>
							<a href="#">Knee Injuries</a>
							<a href="#">Foot/Ankle Injuries</a>
						</div><!-- .side-nav -->
						
					</aside><!-- .sidebar -->
				</div><!-- .cf -->
				
			</div><!-- .sw -->
		</section>

	</article>
	
	<section id="injury-finder">
	
		<section class="light-bg">
			<div class="sw">
				
				<div class="section-header">
					<span class="h3-style">Where Is Your Injury Located?</span>
					<span class="subtitle wide">Nam vehicula est enim, id hendrerit turpis maximus sit amet. Sed pharetra libero et tellus auctor, eu venenatis mi molestie. Phasellus quam lectus, luctus a massa a, ultricies blandit ipsum.</span>
				</div><!-- .section-header -->
				
				<div class="selector" id="injury-finder-selector">
					<select>
						<option value="Back">Back</option>
						<option value="Hip">Hip</option>
						<option value="Knee">Knee</option>
						<option value="Foot/Ankle">Foot/Ankle</option>
					</select><!-- .#injury-selector -->
					<span class="value fa fa-angle-down">&nbsp;</span>
				</div><!-- .selector -->
				
				<div class="grid" id="injury-finder-buttons">
					
					<div class="col-4 col">
						<div class="item">
							<button class="injury-finder-button selected">
								<img src="../assets/images/back.svg" alt="Back Icon">
								<span>Back</span>
							</button>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<button class="injury-finder-button">
								<img src="../assets/images/hip.svg" alt="Hip Icon">
								<span>Hip</span>
							</button>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<button class="injury-finder-button">
								<img src="../assets/images/knee.svg" alt="Knee Icon">
								<span>Knee</span>
							</button>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-4 col">
						<div class="item">
							<button class="injury-finder-button">
								<img src="../assets/images/foot.svg" alt="Foot Icon">
								<span>Foot/Ankle</span>
							</button>
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
			</div><!-- .sw -->
		</section>
		
		<section>
			<div class="filter-area" id="injury-finder-filter-area">
			
				<div class="filter-bar">
					<div class="sw cf">
					
						<div class="filter-controls">
							<button class="prev fa fa-angle-left">Previous</button>
							<button class="next fa fa-angle-right">Next</button>
						</div><!-- .filter-controls -->
						
						<div class="count">
							<span class="num">10</span>
							Results Found
						</div><!-- .count -->
						
					</div><!-- .sw -->
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					<div class="sw">
					
						<div class="grid eqh collapse-no-flex blocks collapse-500">
						
							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<!-- 
										If you do not want the scale effect to happen on the IMG on HOVER,
										add a class of "no-scale" to the block
									-->
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<!-- 
										If you do not want the scale effect to happen on the IMG on HOVER,
										add a class of "no-scale" to the block
									-->
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-4 col sm-col-2">
								<div class="item">
								
									<a class="block with-img no-scale with-button" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/block-head.png);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<h4>Injury Result One</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button blue">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					
					</div><!-- .sw -->
				</div><!-- .filter-content -->
				
			</div><!-- .filter-area -->
		</section>
	
	</section><!-- #injury-finder -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>