var ns = 'BRACE';
window[ns] = {};

// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/custom.select.js";
// @codekit-append "scripts/components/magnific.popup.js"; 
// @codekit-append "scripts/components/gmap.js"; 

// Used on account page
// @codekit-append "scripts/modules/responsive-table/responsive-tables.dev.js

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 
// @codekit-append "scripts/components/swipe.srf.js"; 

// @codekit-append "scripts/search.js"; 
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";

//Used in shop.menu.js
// @codekit-append "scripts/components/raf.polyfill.js"
// @codekit-append "scripts/shop.menu.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";
// @codekit-append "modernizr_tests/android.js";


(function(context) {
	
		//IE8 doesn't support SVGs
		if(!Modernizr.svg) {
			$('img').each(function() {
				var src = this.src;
				if(src.indexOf('.svg') !== -1) {
					this.src = src.replace(/\.svg$/,'.png');
				}
			});
		}

		$(document)
			.on('click','.inline-video',function(e) {
				var 
					el = $(this),
					src = el.data('src');
					
				if(src.length) {
					el
						.removeClass('inline-video')
						.addClass('inline-video-playing')
						.append('<iframe src="'+src+'"/>');
				}
				
			});		

}(window[ns]));