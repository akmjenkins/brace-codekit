(function(context){

	var 
		$document = $(document),
		$body = $('body'),
		$nav = $('nav'),
		SHOW_CLASS = 'show-nav',
		ANIMATING_CLASS = 'animating animating-nav';


	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			methods.toggleNav();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());

	var methods = {

		showNav: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $body.hasClass(SHOW_CLASS);
		},
		
		loadImageForDropdownEl: function(el) {
			var imgURL;
			if(el.length && !el.hasClass('loaded')) {
			
				imgURL = templateJS.templateURL+'/'+el.data('img');
				$('<img/>')
					.on('load',function() {
						el
							.css({
								'background-image':'url('+imgURL+')'
							})
							.addClass('loaded');
					})
					.attr('src',imgURL);
			}
			
		}

	};
	
	$nav
	
		.on('mouseenter','>ul>li',function() {
			methods.loadImageForDropdownEl($(this).find('div.img').eq(0));
		})
	
		.on('mouseenter','ul ul li',function() {
			var
				el = $(this),
				dd = el.closest('div'),
				linkMeta = dd.find('div.link-meta'),
				metaEls = dd.find('div.meta'),
				thisMetaEl = metaEls.eq(el.index()+1),
				thisImgEl = thisMetaEl.siblings('div.img');
				
				linkMeta.addClass('active');
				thisMetaEl.parent().addClass('active');
				
				methods.loadImageForDropdownEl(thisImgEl);				
				
		})
		.on('mouseleave','ul ul li',function() {
			var
				el = $(this),
				dd = el.closest('div'),
				linkMeta = dd.find('div.link-meta'),
				metaEls = dd.find('div.meta');
				
				linkMeta.removeClass('active');
				metaEls.each(function() {
					$(this).parent().removeClass('active');
				});
		})
		

	//no public API
	return {};

}(window[ns]));
