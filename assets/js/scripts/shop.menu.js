(function(context) {

		
		var 
			raf = context.raf,
			$window = $(window),
			$document = $(document),
			$nav = $('div.nav-wrap'),			
			rafDebounce = {
			
				isProcessing: false,
				method: null,
				
				requestProcess: function(method) {
					if(!this.isProcessing) {
						this.method = method;
						this.isProcessing = true;
						raf.requestAnimFrame(this.process,this);
					}
				},
				
				process: function() {
					var self = rafDebounce;
					self.method.apply();
					self.method = null;
					self.isProcessing = false;
				}
				
			},
			shopMenu = {
			
				expandedClass: 'expanded',
				shopMenu: null,
				startOffset:null,
				
				init: function() {
					this.startOffset = this.getShopMenu().offset().top;
					
					//call as soon as document is loaded - which is immediately
					this.onScroll();
				},
				
				onResize: function() {
					if($window.outerWidth() > 1750) {
						this.expand();
					} else {
						this.collapse();
					}
				},
				
				onScroll: function() {
					var 
						scrollPos = $window.scrollTop()+$nav.outerHeight(),
						top = scrollPos > this.startOffset ? scrollPos-this.startOffset : 0;
					
					this.getShopMenu().css('top',top);
					
				},
			
				expand: function() {
					this.getShopMenu().addClass(this.expandedClass);
				},
				
				collapse: function() {
					this.getShopMenu().removeClass(this.expandedClass);
				},
				
				isExpanded: function() {
					return this.getShopMenu().hasClass(this.expandedClass);
				},
				
				toggle: function() {
					if(this.isExpanded()) {
						this.collapse()
					} else {
						this.expand();
					}
				},
				
				getShopMenu: function() {
					if(!this.shopMenu) {
						this.shopMenu = $('div.shop-menu');
					}
					return this.shopMenu;
				}
			};
		
		//only active on pages with shop menu
		if(!shopMenu.getShopMenu().length) {
			return;
		}
		
		$document.on('click','div.shop-menu .fa-navicon',function() { shopMenu.toggle(); });
		$window
			.on('scroll',function() { 
				rafDebounce.requestProcess(function() {
					shopMenu.onScroll(); 
				});
			})
			.on('resize',function() { 
				rafDebounce.requestProcess(function() {
					shopMenu.onResize();
				});
			});
		
		shopMenu.init();
		
		
	//no API
	return {};

}(window[ns]));