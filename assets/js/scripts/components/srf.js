/* SimpleResponsiveFader */
(function($,context) {

	$.fn.SimpleResponsiveFader = function(opts) {			
		
		var SRFInterface = {
			allElements 	: this,	//store the jQuery object passed to the plugin in allElements for easy retrieval
			moveTo 		: function(i) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.moveTo(i); } }); return this; },
			pause			: function() { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.pause(); } }); return this; },
			start				: function(_speed) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.start(_speed); } }); return this; },
			
			//get the raw JavaScript SRF object
			raw				: function() { return this.allElements.data('SimpleResponsiveFader'); },
			
			
			//return to jQuery chainability
			end				: function() { return this.allElements.each(function() { }); }
		}
		
		this.each(function() { if(!$(this).data('SimpleResponsiveFader')) { $(this).data('SimpleResponsiveFader',new SimpleResponsiveFader($(this),opts)); } });	

		return SRFInterface;
		
	}
	
	
	function SimpleResponsiveFader(el,opts) {
		var o = this;
		
		this.state = {
			holder			:	el,
			list				:	el.children('.slider'),
			items			:	el.children('.slider').children(),
			total				:	el.children('.slider').children().length,
			interval			:	false,
			current			:	0,
			controls		:	false
		}
		
		if(!this.state.items.length) { return; }
		
		this.settings = $.extend({},$.fn.SimpleResponsiveFader.defaults,opts);
		this.state.items.eq(0).siblings().css('opacity',0);
		this.addListeners();
		this.state.holder.trigger('SRFInit',[this]);
		this.start();		
		
	}
	
	SimpleResponsiveFader.prototype.addListeners = function() {
		var 
			self 			=	this,
			holder		=	$('<div class="control-div"/>');
			controls 	= 	$('<ul class="slider_controls"/>'),
			links 		= 	$('<ul class="slider_links"/>'),
			linkCb		=	function(e) { self.moveTo($(this).closest('li').index()); $(this).closest('li').addClass('selected').siblings().removeClass('selected'); },
			controlCb	=	function(e) {
				var 
					el 		= 	$(this),
					which	= 	(el.hasClass('next') ? 1 : -1),
					next		=	(+self.state.current)+which,
					actual	=	(next >= self.state.items.length) ? 0 : next;
					
				self.moveTo(actual);
			};
			
		if(this.settings.pauseOnHover) {

			this.state.holder
				.on('mouseenter',function() { self.pause(); })
				.on('mouseleave',function() { self.start(); });
		}
			
		if(this.settings.buildControls && this.state.total > 1) {
			controls
				.append('<li><a class="next">&raquo;</a></li>')
				.append('<li><a class="prev">&laquo;</a></li>');
			
			holder.append(controls);
			this.state.controls = controls.find('a').on('click',controlCb);
		}
		
		if(this.settings.separateControls && this.settings.separateControls.length) { 
			this.settings.separateControls.on('click',controlCb);
		}
		
		if(this.settings.buildLinks && this.state.total > 1) {
			this.state.items.each(function(index,el) { links.append('<li><a>'+((+index)+1)+'</a></li>'); });
			holder.append(links);
			this.state.links = links.find('a').on('click',linkCb);
		}
		
		this.state.total > 1 && this.state.holder.prepend(holder);
		
		if(this.settings.separateLinks && this.settings.separateLinks.length) { this.settings.separateLinks.on('click',linkCb); }
		if(this.settings.centerLinks && this.state.links && this.state.links.length) { links.css('marginLeft',-(this.state.links.closest('ul').outerWidth()/2)).css('left','50%'); }
		
		//add the highlight link class to the selected link
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			links.find('a').eq(this.state.current).parent().addClass(this.settings.highlightLink);
		}
		
	
	}
	
	SimpleResponsiveFader.prototype.pause		=	function() {
		clearInterval(this.state.interval);
		this.state.paused = true;
		this.state.holder.trigger('SRFPause');
	}
	
	SimpleResponsiveFader.prototype.start		=	function(_speed,notrigger) {
		var 
				self 		= 	this,
				next		=	self.state.current-0+1,
				actual	=	(next >= this.state.items.length) ? 0 : next;
		
		this.state.paused = false;
		this.settings.speed = _speed || this.settings.speed;
		this.state.interval = setTimeout(function() { 
			self.moveTo(actual); 
		},self.settings.delay);
		
		if(!notrigger) { this.state.holder.trigger('SRFPause'); }
	}
	
	SimpleResponsiveFader.prototype.moveTo = function(i) {
		var 
			self		=	this,
			next 		= 	this.state.items.eq(i),
			current 	=	this.state.items.eq(this.state.current),
			finish	=	function() {
				next
					.siblings()
					.css('position','absolute')
					.css('opacity',0)
					.css('zIndex',-1)
					.end()
					.css('position','relative')
					.css('zIndex',1);
				
				self.state.animating = false;
				self.state.holder.trigger('SRFFadeEnd',[self]);
				self.state.paused || self.start(false,true);
			};
		
		if(this.state.animating || !next.length || this.state.current === i) { return; }
		clearInterval(this.state.interval);
		
		this.state.current = next.index();
		this.state.holder.trigger('SRFFadeStart',[this]);
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			this.state.links.eq(this.state.current).parent().addClass(this.settings.highlightLink).siblings().removeClass(this.settings.highlightLink);
		}
		this.state.animating = true;
		next.css('display','block').css('position','absolute').css('opacity',0).css('zIndex',10);
		
		if(this.settings.resizeHeight) { this.state.holder.stop().animate({height:next.outerHeight()},this.settings.resizeHeightSpeed,this.settings.resizeHeightEasing); }
		
		switch(this.settings.fadeType) {
			case 'in-out':
				current.stop().animate({opacity:0},this.settings.speed,this.settings.easing,function() {				
					next.stop().animate({opacity:1},self.settings.speed,self.settings.easing,finish);
				});			
				break;
			default:
				next.stop().animate({opacity:1},this.settings.speed,this.settings.easing,finish,function() {
					current.stop().animate({opacity:0},self.settings.speed,self.settings.easing);
				});
				break;
		}
		
	}
	
	
	$.fn.SimpleResponsiveFader.defaults = {
		delay					:	3000,
		easing					:	'swing',
		speed					:	1000,
		pauseOnHover		:	false,
		fadeType				:	'cross',			//string	- cross | in-out				-  	the fader will cross fade, or fade completely out and then in
		
		//animate the height when switching slides?
		resizeHeight				:	false,
		resizeHeightSpeed		:	1000,
		resizeHeightEasing	:	'swing',
		
		//next/previous controls
		buildControls			:	false,
		separateControls	:	false,
		
		
		//1, 2, 3, 4, ... links
		buildLinks				:	false,
		separateLinks		:	false,
		centerLinks			:	true,
		highlightLink			:	'selected'		//class name to be applied to the selected link
	}

	$.extend(context,{
		SimpleResponsiveFader: SimpleResponsiveFader
	});
	
}(jQuery,window[ns]));
