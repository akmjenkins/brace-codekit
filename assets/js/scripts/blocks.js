(function(context) {

		//blocks
		$(window)
			.on('load resize',function() {
				var 
					w = window.innerWidth,
					blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
				
				if(w < 650) {
					blocks.addClass('list-view');
					return;
				}
				
				blocks.each(function(i,blockGroup) {
					var collapseSize = blockGroup.className.match(/collapse-(\d+)/);
					if(collapseSize && collapseSize.length == 2 && w < (+collapseSize[1])) {
						$(blockGroup).addClass('list-view');
					} else {
						$(blockGroup).removeClass('list-view');
					}
				});
				
			}).trigger('resize');
		
}(window[ns]));