(function($,context) {

	$(document).ready(function() {
	  var switched = false;
	  var updateTables = function() {
	    if (($(window).width() < 750) && !switched ){
	      switched = true;
	      $("table.responsive").each(function(i, element) {
	        splitTable($(element));
	      });
	      return true;
	    }
	    else if (switched && ($(window).width() > 750)) {
	      switched = false;
	      $("table.responsive").each(function(i, element) {
	        unsplitTable($(element));
	      });
	    }
	  };
	   
	  $(window).load(updateTables);
	  $(window).bind("resize", updateTables);
	   
		
		function splitTable(original)
		{
			
			if(original.hasClass('hidden')) {
				original.wrap("<div class='table-wrapper hidden' />");
			} else {
				original.wrap("<div class='table-wrapper' />");
			}
			var copy = original.clone();
			copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
			copy.removeClass("responsive");
			
			original.closest(".table-wrapper").append(copy);
			copy.wrap("<div class='pinned' />");
			original.wrap("<div class='scrollable' />");
		}
		
		function unsplitTable(original) {
			var hidden = false;
			var wrapper = original.closest(".table-wrapper");
			wrapper.find(".pinned").remove();
		
			if(wrapper.hasClass('hidden')) {
				hidden = true;
			}
		
			original.unwrap();
			original.unwrap();
			
			hidden && original.addClass('hidden');
		}

	});

}(jQuery,window[ns]));